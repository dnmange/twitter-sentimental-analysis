## Abstract
---

Past research has shown that real-time Twitter data can be used to predict market movement of securities and other financial instruments.     

The goal of this project is  

1. To Build a model for six major U.S. airlines that performs sentiment analysis on customer reviews so that the airlines can have fast and concise feedback.  
2. And Make recommendations on the most important aspect of services they could improve given customers complains.   
    In this project, we performed multi-class classification using following Classifiers:
   ```
    Naive Bayes,   
    SVM,  
    Logistic Regression,  
    Random Forest,  
    Decision Tree,   
    K-Nearest Neighbors``` 
    on the Twitter US Airline data set from Kaggle. Significant accuracy has achieved, which shows that our models are reliable for future prediction. Also, the accuracy of different models is compared, and results show that Random Forest is the best approach.  

Click on the [report](report/ml-report.pdf) to read more in details
