from __future__ import division

import re

import matplotlib.pyplot as varPlot
import nltk
import pandas
from nltk.corpus import stopwords
from sklearn.model_selection import train_test_split
from sklearn.ensemble import GradientBoostingClassifier, AdaBoostClassifier
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import accuracy_score
from sklearn.tree import DecisionTreeClassifier

Tweet = pandas.read_csv('Tweets.csv')
Tweet.head()

(len(Tweet)-Tweet.count())/len(Tweet)
del Tweet['tweet_coord']
del Tweet['airline_sentiment_gold']
del Tweet['negativereason_gold']


def new_tweet_length(unchanged_tweet):

    only_alphabets = re.sub("[^a-zA-Z]", " ", unchanged_tweet)
    words = only_alphabets.lower().split()
    stops = set(stopwords.words("english"))                  
    refined_words = [w for w in words if not w in stops]
    return len(refined_words)


# 2 gram of models is used to extract the features example: ["not good","is not"]
def convert_to_words(unchanged_tweet):

    only_alphabets = re.sub("[^a-zA-Z]", " ", unchanged_tweet)
    words = only_alphabets.lower().split()
    stops = set(stopwords.words("english"))
    refined_words = [w for w in words if not w in stops]
    bigram_feature_vector = []

    for item in nltk.bigrams(refined_words):
        bigram_feature_vector.append(' '.join(item))

    return str(bigram_feature_vector)


Tweet['sentiment'] = Tweet['airline_sentiment'].apply(lambda x: 0 if x=='negative' else(1 if x=='positive' else 2))
Tweet['refinedTweets'] = Tweet['text'].apply(lambda x: convert_to_words(x))
Tweet['tweet_length'] = Tweet['text'].apply(lambda x: new_tweet_length(x))

train, test = train_test_split(Tweet, test_size=0.2, random_state=42)

train_refined_tweets = []
for tweet in train['refinedTweets']:
    train_refined_tweets.append(tweet)

test_refined_tweets=[]
for tweet in test['refinedTweets']:
    test_refined_tweets.append(tweet)

v = CountVectorizer(analyzer = "word")
train_features = v.fit_transform(train_refined_tweets)
test_features = v.transform(test_refined_tweets)

classifier_list = [
 GradientBoostingClassifier(n_estimators=100, learning_rate=1.0,max_depth=4, random_state=0),
 AdaBoostClassifier(base_estimator=DecisionTreeClassifier(max_depth=18, min_samples_leaf=25,
                                                          min_samples_split=10), n_estimators=10)]

advance_features = train_features.toarray()
advance_features_test = test_features.toarray()
classifier_accuracy_list = []
classifier_model = []

for classifier in classifier_list:
    try:

        fit = classifier.fit(train_features, train['sentiment'])
        predicted = fit.predict(test_features)
    except Exception:

        fit = classifier.fit(advance_features, train['sentiment'])
        predicted = fit.predict(advance_features_test)

    accuracy = accuracy_score(predicted, test['sentiment'])

    classifier_accuracy_list.append(accuracy)
    classifier_model.append(classifier.__class__.__name__)

    print('Accuracy of '+classifier.__class__.__name__+' is '+str(accuracy))

positions = [1,2,3,4,5,6,7]
varPlot.bar(positions, classifier_accuracy_list)
varPlot.xticks(positions, classifier_model, rotation=45)
varPlot.ylabel('Accuracy')
varPlot.xlabel('Classifier Model')
varPlot.title('Accuracies of ClassifierModels')
